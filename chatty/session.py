"""
Abstraction for dealing with connection: read and write data.

Check if session is alive it sends data, in another case it uses fallback.

If you add new low-level connection, e.g. UDP or CoAP, here should one more
realization to provide reliable interface for another components, which sends
data.
"""
import abc


class BaseSessionError(Exception):
    """Base session exception."""


class WSSessionError(BaseSessionError):
    """WebSocker session exception."""


class BaseSession(abc.ABC):
    """Base class for user connection"""

    @abc.abstractmethod
    async def _send(self, data: bytes) -> None:
        """Send byte-data."""
        ...

    @abc.abstractmethod
    async def _fallback(self, data):
        """Deal with data in case if connection already closed."""
        ...

    @abc.abstractmethod
    async def is_alive(self):
        """Check if session is alive."""
        ...

    @abc.abstractmethod
    async def receive(self):
        """Receive data from connection."""
        ...

    async def send(self, data):
        """Check if connection is alive and send data."""
        if self.is_alive():
            return await self._send(data)
        else:
            return await self._fallback(data)


class WebSocketSession(BaseSession):

    def __init__(self, ws):
        """Constructor for WebSocket session."""
        self._ws = ws

    def is_alive(self) -> bool:
        """Check if session is alive."""
        return not self._ws.closed

    async def receive(self) -> bytes:
        """
        Receive data from connection.

        :raises WSSessionError: if non binary data was received.
        """
        try:
            return await self._ws.receive_bytes()
        except TypeError:
            raise WSSessionError

    async def close(self) -> bool:
        """Close connection."""
        return await self._ws.close()

    async def _send(self, data: bytes) -> None:
        """Send binary data in socket."""
        await self._ws.send_bytes(data)

    async def _fallback(self, data: bytes) -> None:
        """Log something if session is closed."""
        # TODO: log here
        ...
