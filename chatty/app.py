import logging
from typing import Any, Dict

from aiohttp import web

from . import pubsub, storage
from .routers import prepare_router

log = logging.getLogger('chatty.app')


async def prepare_app(config: Dict[Any, Any]) -> web.Application:
    app = web.Application(logger=log, router=prepare_router())
    await storage.setup(app, config['storage'])
    await pubsub.setup(app, config['pubsub'])
    return app
