from datetime import datetime
from typing import Any

from .pubsub import PubSuber
from .storage import Storage


async def auth(
        storage: Storage, pubsuber, login: str, token: str, session) -> bool:
    if not await storage.check_auth_token(login, token):
        return False
    await storage.user_connected(login)
    pubsuber.add_session(login, session)
    return True


async def disconnect(
        storage: Storage, pubsuber: PubSuber, login: str, session) -> None:
    await storage.user_disconnected(login)
    pubsuber.del_session(login, session)


async def online_user_list(storage: Storage) -> Any:
    return await storage.get_online_user_list()


async def new_user_message(
        storage: Storage, pubsuber: PubSuber, message: Any) -> None:
    message.datetime = datetime.utcnow().timestamp()
    await storage.save_message(message)
    pubsuber.publish_new_message(message)
