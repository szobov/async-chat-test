import socket
import subprocess
import time

import pytest
from aioredis import Redis, create_redis

from ..app import prepare_app


def __unused_port():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind(('127.0.0.1', 0))
        return s.getsockname()[1]


@pytest.yield_fixture(scope='session')
def redis_server():
    redis_port = __unused_port()
    process = subprocess.Popen(
        ['docker', 'run', '--rm', '-p', f'{redis_port}:6379', 'redis:alpine'],
        stdout=subprocess.DEVNULL
    )
    while 1:
        _p = subprocess.run(
            ['redis-cli', '-h', '127.0.0.1', '-p', f'{redis_port}',
             'COMMAND', 'INFO'],
            stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT
        )
        if _p.returncode == 0:
            break
        time.sleep(0.1)
    yield redis_port
    process.terminate()
    process.wait()


@pytest.fixture(scope='session')
def app_config(redis_server):

    return {
        'storage': {
            'redis': {
                'address': ['127.0.0.1', redis_server],
                'db': 1,
                'timeout': 30,
                'encoding': 'utf-8'
            },
        },
        'pubsub': {
            'redis': {
                'address': ['127.0.0.1', redis_server],
                'db': 2,
                'timeout': 30,
            },
        }
    }


@pytest.fixture
def app(loop, app_config):
    return loop.run_until_complete(prepare_app(app_config))


@pytest.yield_fixture
def client(loop, aiohttp_client, aiohttp_unused_port, app):
    yield loop.run_until_complete(
        aiohttp_client(app, server_kwargs={'port': aiohttp_unused_port()}))


@pytest.fixture
def default_user_dict():
    return {'login': 'foo', 'password': 'bar'}


@pytest.yield_fixture
async def redis(app_config):
    cli = await create_redis(**app_config['storage']['redis'])
    yield cli
    await cli.flushall()
