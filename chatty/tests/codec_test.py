from binascii import crc32

import pytest

from .. import codec
from ..proto import api_pb2


@pytest.fixture
def auth_message():
    mess = api_pb2.Auth()
    mess.login = "foo_bar"
    mess.token = "supapupatoken"
    return mess


@pytest.fixture
def message_from_user():

    def inner(is_direct=False):
        mess = api_pb2.MessageFromUser()
        mess.data = 'Hello world! blah-blah.'
        mess.user_token = 'supadupatoken'
        mess.user_login = 'foo_bar'
        mess.datetime = 123456.321
        if is_direct:
            mess.to_user = 'bar_foo'
        return mess

    return inner


@pytest.fixture
def pack_message():

    def inner(message, type=api_pb2.AUTH):
        byte_message = message.SerializeToString()
        packed = api_pb2.PackedMessage()
        packed.type = type
        packed.payload = byte_message
        packed.check_sum = crc32(byte_message)
        return packed
    return inner


@pytest.fixture
def unpack_message():

    def inner(data):
        unpacked = api_pb2.PackedMessage()
        unpacked.ParseFromString(data)
        return unpacked
    return inner


class TestUnpackMessage:

    def test_ok(self, auth_message, pack_message):
        packed = pack_message(auth_message)
        expected = (codec.IncomingMessageType.AUTH, auth_message)
        unpacked = codec.unpack_message(packed.SerializeToString())
        assert expected == unpacked
        assert unpacked.message.login == auth_message.login
        assert unpacked.message.token == auth_message.token

    def test_bad_package(self):
        with pytest.warns(RuntimeWarning):
            with pytest.raises(codec.CodecDecodeError) as ex:
                data = b'\x00\xFF\x11\xFE'
                codec.unpack_message(data)
                assert ex.message == 'Cannot parse packed message'

    def test_corrupted(self, auth_message, pack_message):
        packed = pack_message(auth_message)
        packed.payload = b'\x00\xFF\x11\xFE'
        with pytest.raises(codec.CodecCorruptedMessage):
            codec.unpack_message(packed.SerializeToString())

    def test_unexpected_message(self, auth_message, pack_message):
        packed = pack_message(auth_message)
        packed.type = api_pb2.MESSAGE_TO_USER
        with pytest.raises(codec.CodecUnexpectedMessage):
            codec.unpack_message(packed.SerializeToString())

    def test_can_not_decode_payload(self, auth_message, pack_message):
        broken_mess = auth_message.SerializeToString()
        broken_mess = broken_mess[:-len(auth_message.token)-2]
        packed = api_pb2.PackedMessage()
        packed.type = api_pb2.AUTH
        packed.payload = broken_mess
        packed.check_sum = crc32(broken_mess)
        with pytest.raises(codec.CodecDecodeError) as ex:
            codec.unpack_message(packed.SerializeToString())
            assert ex.message == 'Cannot parse payload'


class TestPackMessage:

    def test_ok(self, auth_message, pack_message):
        packed = pack_message(auth_message).SerializeToString()
        assert packed == codec.pack_message(auth_message, api_pb2.AUTH)

    def test_incompleted_message(self, auth_message):
        auth_message.ClearField('token')
        with pytest.raises(codec.CodecEncodeError):
            codec.pack_message(auth_message, api_pb2.AUTH)


def test_get_bytes(auth_message):
    expected = auth_message.SerializeToString()
    assert expected == codec.get_byte_message(auth_message)


@pytest.mark.parametrize('is_direct', [True, False])
def test_prepare_message_to_user(message_from_user, unpack_message, is_direct):
    mess_from_user = message_from_user(is_direct)
    prepared = codec.prepare_message_to_user(mess_from_user, is_direct)
    unpacked = unpack_message(prepared)
    assert unpacked.type == api_pb2.MESSAGE_TO_USER
    mess = api_pb2.MessageToUser()
    mess.ParseFromString(unpacked.payload)
    assert mess.from_user == mess_from_user.user_login
    assert mess.data == mess_from_user.data
    assert mess.datetime == mess_from_user.datetime
    assert mess.is_direct == is_direct


@pytest.mark.parametrize('success, expected_result', [
    (True, api_pb2.AuthResponse.OK),
    (False, api_pb2.AuthResponse.ERROR)
])
@pytest.mark.parametrize('details', ['', 'FooBar!'])
def test_prepare_auth_response(unpack_message, success, expected_result,
                               details):
    prepared = codec.prepare_auth_response(success, details)
    unpacked = unpack_message(prepared)
    assert unpacked.type == api_pb2.AUTH_RESULT
    mess = api_pb2.AuthResponse()
    mess.ParseFromString(unpacked.payload)
    assert mess.result == expected_result
    assert mess.details == details


@pytest.mark.parametrize('user_list', [[], ['foo_bar', 'bar_foo']])
def test_prepate_online_user_list(user_list, unpack_message):
    prepared = codec.prepare_online_user_list(user_list)
    unpacked = unpack_message(prepared)
    assert unpacked.type == api_pb2.ONLINE_USER_LIST
    mess = api_pb2.OnlineUserList()
    mess.ParseFromString(unpacked.payload)
    assert mess.user_login == user_list


def test_get_auth_message(auth_message, message_from_user, pack_message):
    packed = pack_message(auth_message).SerializeToString()
    assert auth_message == codec.get_auth_message(packed)
    unexpected_message = pack_message(
        message_from_user(), api_pb2.MESSAGE_FROM_USER).SerializeToString()
    with pytest.raises(codec.CodecUnexpectedMessage):
        codec.get_auth_message(unexpected_message)


@pytest.mark.parametrize('is_direct,expected_direction', [
    (True, codec.MessageDirection.TO_USER),
    (False, codec.MessageDirection.TO_EVERYONE)
])
def test_parse_user_message(message_from_user, is_direct, expected_direction):
    prepared_mess = message_from_user(is_direct)
    byte_mess = prepared_mess.SerializeToString()
    direction, message = codec.parse_user_message(byte_mess)
    assert direction == expected_direction
    assert message == prepared_mess
