import pytest

from ..storage import Storage
from ..utils import generate_hashed_password


@pytest.fixture
def storage(client, app):
    return app['Storage']


@pytest.fixture
def create_user(storage, loop):

    async def f(login, password):
        await storage.create_user(login, password)

    return f


async def test_create_user(redis, storage: Storage, create_user,
                           default_user_dict):
    await create_user(**default_user_dict)
    assert await redis.hget(
        storage._get_user_key(default_user_dict['login']),
        'password') == generate_hashed_password(default_user_dict['password'])


async def test_auth_user_ok(redis, storage: Storage,
                            create_user, default_user_dict):
    await create_user(**default_user_dict)
    token = await storage.auth_user(**default_user_dict)
    assert token == await redis.hget(
        storage._get_user_key(default_user_dict['login']),
        'auth_token')


async def test_check_user(storage: Storage,
                          create_user, default_user_dict):
    await create_user(**default_user_dict)
    token = await storage.auth_user(**default_user_dict)

    async def call(user, tok):
        return await storage.check_auth_token(user, tok)

    assert await call(default_user_dict['login'], token)
    assert not await call(default_user_dict['login'], 'foobar')
    assert not await call('foobar', token)
