import json

import pytest

from .. import codec
from ..proto import api_pb2


async def test_sign_up(client, default_user_dict):
    resp = await client.post('/sign-up', data=json.dumps(default_user_dict))
    assert resp.status == 200


@pytest.fixture
async def user_token(client, redis, default_user_dict):
    await client.post('/sign-up', data=json.dumps(default_user_dict))
    resp = await client.post('/sign-in', data=json.dumps(default_user_dict))
    data = await resp.json()
    return data['auth_token']


@pytest.fixture
def auth_message(default_user_dict, user_token):
    mess = api_pb2.Auth()
    mess.login = default_user_dict['login']
    mess.token = user_token
    return codec.pack_message(mess, api_pb2.AUTH)


@pytest.fixture
def mess_from_user(default_user_dict, user_token):
    mess = api_pb2.MessageFromUser()
    mess.data = 'こんにちは world!'
    mess.user_token = user_token
    mess.user_login = default_user_dict['login']
    return mess


@pytest.fixture
def unpack_message():

    def inner(data, mess_class):
        unpacked = api_pb2.PackedMessage()
        unpacked.ParseFromString(data)
        mess = mess_class()
        mess.ParseFromString(unpacked.payload)
        return mess
    return inner


@pytest.fixture
def storage_get_user_prop(app, redis):
    async def inner(user_login, prop):
        return await redis.hget(app['Storage']._get_user_key(user_login), prop)
    return inner


class TestWebSocketHandler:

    async def test_ok(self, client, auth_message, unpack_message,
                      mess_from_user, default_user_dict, app,
                      storage_get_user_prop):
        ws = await client.ws_connect('/chat-websockets')
        await ws.send_bytes(auth_message)
        auth_response = unpack_message(
            await ws.receive_bytes(), api_pb2.AuthResponse)
        assert auth_response.result == api_pb2.AuthResponse.OK
        assert await storage_get_user_prop(
            default_user_dict['login'], 'active_sessions') == '1'

        await ws.send_bytes(
            codec.pack_message(mess_from_user, api_pb2.MESSAGE_FROM_USER))
        mess_to_user = unpack_message(
            await ws.receive_bytes(), api_pb2.MessageToUser)
        assert mess_to_user.from_user == default_user_dict['login']
        assert mess_to_user.data == mess_from_user.data
        assert not mess_to_user.is_direct
        assert await ws.close()
        assert await storage_get_user_prop(
            default_user_dict['login'], 'active_sessions') == '0'
        assert len(
            app['PubSuber']._subscribers[default_user_dict['login']]) == 0
