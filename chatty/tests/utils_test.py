import pytest

from ..utils import (generate_auth_token, generate_hashed_password,
                     verify_password)


@pytest.mark.parametrize("password1, password2, expected", [
    ('foo', 'foo', True),
    ('foo', 'bar', False)
])
def test_generate_password(password1, password2, expected):
    hashed = generate_hashed_password(password1)
    assert verify_password(password2, hashed) == expected


def test_generate_auth_token():
    assert type(generate_auth_token()) == str
    assert generate_auth_token() != generate_auth_token()
