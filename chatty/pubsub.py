"""
PubSuber -- connector for publishing and subscring data and process it.

It uses redis for exchange. In background task it starts asyncronius listenner,
that list for published messages and processes them. Also PubSuber has method
for publish data.
"""
import logging
from asyncio import CancelledError, Task
from collections import defaultdict
from typing import Any, Dict, Mapping

from aiohttp import web
from aioredis import Redis, create_redis, Channel
from . import codec
from .session import BaseSession

log = logging.getLogger('pubsuber')


async def setup(
        app: web.Application, config: Dict[Any, Any]
) -> web.Application:
    """Setup PubSuber for web app."""

    async def startup(app: web.Application):
        app['PubSuber'] = PubSuber(config['redis'])
        await app['PubSuber'].run(app.loop)

    async def cleanup(app: web.Application):
        await app['PubSuber'].cancel()

    app.on_startup.append(startup)
    app.on_cleanup.append(cleanup)
    return app


class PubSuber:

    _MESSAGES_PATTERN = 'MESSAGES:'

    def __init__(self, redis_config: Dict[Any, Any]) -> None:
        """
        Constructor for PubSuber.

        It just prepares variables for initialization and saves redis config.
        """
        self._redis_config = redis_config
        self._subscribers: Mapping = defaultdict(list)
        self._message_channel: Channel = None
        self._listen_task: Task = None

    async def run(self, loop) -> None:
        """
        Run pubsuber.

        * Create redis pub and sub connections.
        * Subscribe on message channel.
        * Start background task for processing published data.
        """
        self._redis_sub: Redis = await create_redis(**self._redis_config)
        self._redis_pub: Redis = await create_redis(**self._redis_config)
        [self._message_channel, *_] = await self._redis_sub.psubscribe(
            self._MESSAGES_PATTERN)
        self._listen_task = loop.create_task(self._listen())

    async def _listen(self) -> None:
        """
        Asyncio task for Listen for published message and process them.

        If task would be destroyed it closes pub and sub redis connections.
        """
        try:
            async for (_, data) in self._message_channel.iter():
                await self._process_data(data)
        except CancelledError:
            self._redis_sub.close()
            await self._redis_sub.wait_closed()
            self._redis_pub.close()
            await self._redis_pub.wait_closed()

    async def cancel(self) -> None:
        """Cancel background listener task."""
        self._listen_task.cancel()
        await self._listen_task

    def add_session(self, user: str, session: BaseSession) -> None:
        """Add one user sessions to user sessions list."""
        self._subscribers[user].append(session)

    def del_session(self, user: str, session: BaseSession) -> None:
        """Delete one user sessions from user sessions list."""
        self._subscribers[user].remove(session)

    async def _process_data(self, data: bytes) -> None:
        """
        Process message from user and decide to whoam send it.

        Sends it directly to user or to all users.
        """
        direction, message = codec.parse_user_message(data)
        if direction == codec.MessageDirection.TO_USER:
            await self._process_direct_message(message)
        else:
            await self._process_message_to_everyone(message)

    async def _process_direct_message(self, message: Any) -> None:
        """Send one message to user, using all current users sessions."""
        for session in self._subscribers[message.to_user]:
            log.debug('Send direct message %s to user')
            prepared = codec.prepare_message_to_user(message, direct=True)
            await session.send(prepared)

    async def _process_message_to_everyone(self, message: Any) -> None:
        """
        Walk through all users sessions and send message.

        :XXX Suppose, it's not a best idea to walk through all sessions in one
        cycle.
        """
        prepared = codec.prepare_message_to_user(message, direct=False)
        log.debug('Send message %s to users', prepared)
        for sessions in self._subscribers.values():
            for session in sessions:
                await session.send(prepared)

    def publish_new_message(self, message: Any) -> None:
        """Publish new message from user, using publish redis channel"""
        self._redis_pub.publish(
            self._MESSAGES_PATTERN, codec.get_byte_message(message))
