"""Application handlers."""
import logging

from aiohttp import web

from . import chat_processor, codec
from .pubsub import PubSuber
from .session import WebSocketSession, WSSessionError
from .storage import Storage
from .utils import validate_request

log = logging.getLogger('handlers')

_sign_schema = {'login': {'type': 'string'}, 'password': {'type': 'string'}}


@validate_request(_sign_schema)
async def sign_up(storage: Storage, login: str, password: str):
    """Register new user."""
    await storage.create_user(login, password)


@validate_request(_sign_schema)
async def sign_in(storage: Storage, login: str, password: str):
    """Authorize user and get authentification token."""
    return {'auth_token': await storage.auth_user(login, password)}


async def websocket_handler(request: web.BaseRequest) -> web.WebSocketResponse:
    """
    Handle websocket connection and process incoming messages.

    Method provides chat logic: authentificate user, receive incoming messages
    and send messages to him.
    """

    ws = web.WebSocketResponse()
    await ws.prepare(request)

    session = WebSocketSession(ws)
    try:
        auth_mess = codec.get_auth_message(await session.receive())
    except (codec.BaseCodecError, WSSessionError):
        await session.close()
        return ws

    login: str = auth_mess.login
    token: str = auth_mess.token

    storage: Storage = request.app['Storage']
    pubsuber: PubSuber = request.app['PubSuber']

    if not await chat_processor.auth(
            storage, pubsuber, login, token, session):
        await session.send(codec.prepare_auth_response(False))
        await session.close()
        return ws

    await session.send(codec.prepare_auth_response(True))

    while 1:
        try:
            mess_type, message = codec.unpack_message(await session.receive())

            if mess_type == codec.IncomingMessageType.GET_ONLINE_USER_LIST:
                user_list = await chat_processor.online_user_list(storage)
                await session.send(codec.prepare_online_user_list(user_list))

            if mess_type == codec.IncomingMessageType.MESSAGE_FROM_USER:
                await chat_processor.new_user_message(
                    storage, pubsuber, message)
        except codec.BaseCodecError:
            log.warning('Bizarre message was received. Can not decode it.')
            break
        except WSSessionError:
            log.warning('Errors while prossing error.')
            break

    await chat_processor.disconnect(
        storage, pubsuber, login, session)
    await session.close()
    return ws
