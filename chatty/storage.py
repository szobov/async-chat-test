"""
Storage incapsulates function for work with data storage.

Underhood it uses redis as a database.
"""
import logging
from typing import Any, Dict, List

from aioredis import create_redis
from aioredis.commands import Redis
from aiohttp import web

from .utils import (generate_auth_token, generate_hashed_password,
                    verify_auth_token, verify_password)

log = logging.getLogger('chatty.storage.redis')


async def setup(app: Any, config: Dict[Any, Any]) -> web.Application:
    """Setup storage for web app."""

    async def startup(app):
        redis = await create_redis(**config['redis'])
        app['Storage'] = Storage(redis)

    async def cleanup(app):
        app['Storage'].redis.close()
        await app['Storage'].redis.wait_closed()

    app.on_startup.append(startup)
    app.on_cleanup.append(cleanup)
    return app


class Storage:

    _USER_PREFIX = 'user:{login}'
    _USER_MESSAGES_PREFIX = 'user_messages:{message_id}'

    def __init__(self, redis: Redis) -> None:
        """Constructor for storage."""
        self.redis = redis

    def _get_user_key(self, login: str) -> str:
        """Return full user key for access it in redis."""
        return self._USER_PREFIX.format(login=login)

    def _get_message_key(self, message_id: str) -> str:
        """Return full message key for access it in redis."""
        return self._USER_MESSAGES_PREFIX.format(message_id=message_id)

    async def create_user(self, login: str, password: str) -> None:
        """Create new user."""
        await self.redis.hmset_dict(
            self._get_user_key(login), {
                'password': generate_hashed_password(password),
            })
        log.debug('user was created')

    async def auth_user(self, login: str, password: str) -> str:
        """Check user login and password and return auth token."""
        user_key = self._get_user_key(login)
        user_pass, auth_token = await self.redis.hmget(
            user_key, 'password', 'auth_token')

        if not verify_password(password, user_pass):
            log.debug('user %s was not founded', login)
            raise

        if auth_token:
            log.debug('user %s is already authentificated', login)
            return auth_token

        auth_token = generate_auth_token()
        self.redis.hset(user_key, 'auth_token', auth_token)
        log.debug('user %s is authentificated', login)
        return auth_token

    async def check_auth_token(self, login: str, token: str) -> bool:
        """Check auth token for user."""
        user_key = self._get_user_key(login)
        auth_token = await self.redis.hget(user_key, 'auth_token')
        if auth_token is None:
            log.debug('user %s is not authentificated')
            return False
        return verify_auth_token(token, auth_token)

    async def user_connected(self, login: str):
        """Increase user sessions count."""
        log.debug('Increase user (%s) active sessions count', login)
        await self.redis.hincrby(
            self._get_user_key(login), 'active_sessions', 1)

    async def user_disconnected(self, login: str):
        """Decrease user sessions count."""
        log.debug('Decrease user (%s) active sessions count', login)
        await self.redis.hincrby(
            self._get_user_key(login), 'active_sessions', -1)

    async def get_online_user_list(self) -> List[str]:
        """Get list of currently connected uses."""
        user_list: List[str] = []
        async for key in self.redis.iscan(match='{}*'
                                          .format(self._USER_PREFIX)):
            active = await self.redis.hget(key, 'active_sessions')
            if active and active > 0:
                user_list.append(key[len(self._USER_PREFIX):])
        return user_list

    async def save_message(self, message: Any) -> None:
        """Save message info."""
        ...
