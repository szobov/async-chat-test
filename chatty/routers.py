"""Prepare web-appication router."""
from aiohttp.web import UrlDispatcher

from .handlers import sign_in, sign_up, websocket_handler


def prepare_router() -> UrlDispatcher:
    """
    Prepare handlers for application.

    Add http handlers to deal with user.
    Add websocket handler to provide chat logic.
    """
    router = UrlDispatcher()
    handlers = (
        ('POST', '/sign-up', sign_up),
        ('POST', '/sign-in', sign_in),
        ('GET', '/chat-websockets', websocket_handler)
    )
    for method, path, handler in handlers:
        router.add_route(method, path, handler)
    return router
