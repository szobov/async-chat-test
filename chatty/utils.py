from functools import wraps
from hashlib import blake2b
from hmac import compare_digest
from json import JSONDecodeError, dumps
from os import urandom
from typing import Any, Callable, Dict

from aiohttp import web
from cerberus import Validator


def validate_request(schema: Dict[str, Dict[str, str]]) -> Callable[..., Any]:
    """
    Factory of validation decorators.

    Validate request's parameters and pass it to handler function with
    storage object.
    """

    Validator(schema)

    def wrapper(handler: Callable[..., Any]) -> Callable[..., Any]:

        @wraps(handler)
        async def wrapped(req: web.BaseRequest) -> web.Response:
            validator = Validator(schema)
            try:
                body = await req.json()
            except JSONDecodeError:
                raise web.HTTPBadRequest(body='Can not decode requests body')
            validated = validator.validated(body)
            if validated is None:
                raise web.HTTPBadRequest(body=dumps(validator.errors))
            return web.Response(
                body=dumps(await handler(req.app['Storage'], **validated)),
                content_type='application/json')

        return wrapped

    return wrapper


def generate_hashed_password(password: str) -> str:
    hashed = blake2b()
    hashed.update(password.encode('utf-8'))
    return hashed.hexdigest()


def verify_password(maybe_password: str, hashed_password: str) -> bool:
    hashed = generate_hashed_password(maybe_password)
    return compare_digest(hashed, hashed_password)


def generate_auth_token() -> str:
    return blake2b(salt=urandom(blake2b.SALT_SIZE)).hexdigest()


def verify_auth_token(maybe_token: str, token: str) -> bool:
    return compare_digest(maybe_token, token)
