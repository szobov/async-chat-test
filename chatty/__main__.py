import logging
from asyncio import AbstractEventLoop, get_event_loop

import click
import yaml
from aiohttp import web
from aiohttp.web_runner import GracefulExit
from frozendict import frozendict

from .app import prepare_app

log = logging.getLogger('chatty')
log.setLevel(logging.DEBUG)


def run(config: frozendict) -> None:
    loop: AbstractEventLoop = get_event_loop()
    runner = web.AppRunner(loop.run_until_complete(prepare_app(config)))
    loop.run_until_complete(runner.setup())
    site = web.TCPSite(runner,
                       config['server']['host'], config['server']['port'])

    loop.run_until_complete(site.start())
    try:
        log.debug('Running application')
        loop.run_forever()
    except (GracefulExit, KeyboardInterrupt):
        log.debug('Stopping application')
    finally:
        loop.run_until_complete(runner.cleanup())


@click.command()
@click.option('config_path', '--config', '-c', type=click.Path())
def main(config_path: str):
    with open(config_path, 'r') as f:
        config: frozendict = frozendict(yaml.load(f))
    run(config)


if __name__ == '__main__':
    main()
