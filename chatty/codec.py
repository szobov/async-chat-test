"""
Provide classes to deal with protobuf message.

In case we changed protobuf on another protocol it should save interface.
"""
from binascii import crc32
from collections import namedtuple
from enum import Enum
from typing import List, Tuple

from google.protobuf import message

from .proto import api_pb2 as api_proto


class BaseCodecError(Exception):
    """Base codec exception."""


class CodecEncodeError(BaseCodecError):
    """Error while encoding message to binary representation."""


class CodecDecodeError(BaseCodecError):
    """Error while decoding data."""


class CodecCorruptedMessage(BaseCodecError):
    """Bad message check sum."""


class CodecUnexpectedMessage(BaseCodecError):
    """Unexpected message type."""


def pack_message(
        mess: message.Message, mess_type: api_proto.MessageType
) -> bytes:
    """Pack potobuf message."""
    packed = api_proto.PackedMessage()
    packed.type = mess_type
    try:
        payload = mess.SerializeToString()
    except message.EncodeError:
        raise CodecEncodeError
    packed.payload = payload
    packed.check_sum = crc32(payload)
    return packed.SerializeToString()


class IncomingMessageType(Enum):
    """Incoming message type."""
    AUTH = api_proto.AUTH
    GET_ONLINE_USER_LIST = api_proto.GET_ONLINE_USER_LIST
    MESSAGE_FROM_USER = api_proto.MESSAGE_FROM_USER


type_to_message_class = {
    IncomingMessageType.AUTH:  api_proto.Auth,
    IncomingMessageType.GET_ONLINE_USER_LIST: api_proto.GetOnlineUserList,
    IncomingMessageType.MESSAGE_FROM_USER: api_proto.MessageFromUser
}

unpacked_message_result = namedtuple(
    'unpacked_message_result', ['type', 'message'])


def unpack_message(data: bytes) -> Tuple[IncomingMessageType, message.Message]:
    """Unpack message."""
    unpacked = api_proto.PackedMessage()
    unpacked.ParseFromString(data)
    if not unpacked.IsInitialized():
        raise CodecDecodeError('Cannot parse packed message')
    if crc32(unpacked.payload) != unpacked.check_sum:
        raise CodecCorruptedMessage
    try:
        mess_type = IncomingMessageType(unpacked.type)
    except ValueError:
        raise CodecUnexpectedMessage

    mess = type_to_message_class[mess_type]()
    mess.ParseFromString(unpacked.payload)
    if not mess.IsInitialized():
        raise CodecDecodeError('Cannot parse payload')

    return unpacked_message_result(mess_type, mess)


def get_byte_message(message: message.Message) -> bytes:
    """Get bytes from protobuf message class."""
    return message.SerializeToString()


def prepare_message_to_user(
        user_message: api_proto.MessageFromUser, direct: bool
) -> bytes:
    """Prepare packed to user."""
    server_message = api_proto.MessageToUser()
    server_message.from_user = user_message.user_login
    server_message.data = user_message.data
    server_message.datetime = user_message.datetime
    server_message.is_direct = direct
    return pack_message(server_message, api_proto.MESSAGE_TO_USER)


def prepare_auth_response(
        success: bool, details: str ='') -> api_proto.PackedMessage:
    """Prepare packed auth message."""
    mess = api_proto.AuthResponse()
    if success:
        mess.result = api_proto.AuthResponse.OK
    else:
        mess.result = api_proto.AuthResponse.ERROR
    mess.details = details
    return pack_message(mess, api_proto.AUTH_RESULT)


def prepare_online_user_list(
        user_list: List[str]) -> api_proto.OnlineUserList:
    """Prepare packed user online list message."""
    mess = api_proto.OnlineUserList()
    mess.user_login.extend(user_list)
    return pack_message(mess, api_proto.ONLINE_USER_LIST)


def get_auth_message(data: bytes) -> api_proto.Auth:
    """Get auth message from bytes date."""
    mess_type, mess = unpack_message(data)
    if mess_type != IncomingMessageType.AUTH:
        raise CodecUnexpectedMessage
    return mess


class MessageDirection(Enum):
    """User message direction."""

    TO_USER = 'to_user'
    TO_EVERYONE = 'to_everyone'


def parse_user_message(
        data: bytes) -> Tuple[MessageDirection, api_proto.MessageFromUser]:
    """Parse binary data and get user message direction and data."""
    mess: message.Message = api_proto.MessageFromUser()
    mess.ParseFromString(data)
    if not mess.IsInitialized():
        raise CodecDecodeError
    if mess.HasField('to_user'):
        return MessageDirection.TO_USER, mess
    return MessageDirection.TO_EVERYONE, mess
