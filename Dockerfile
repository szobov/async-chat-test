FROM frolvlad/alpine-python3

ADD . /dist/
RUN apk add --no-cache gcc g++ make libffi-dev python3-dev \
    && cd dist \
    && python setup.py sdist \
    && pip install dist/* \
    && cd / \
    && apk del gcc g++ make libffi-dev python3-dev \
    && rm -rf /dist/ /root/.cache/ \
    && mkdir /config
VOLUME ["/config"]

ENTRYPOINT ["chatty_run", "--config", "/config/config.yaml"]
