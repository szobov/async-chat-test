Test assignment for "Vispa media".

**Task**:

Develop backend-server for chat with user registration using websockets and redis.

**Functional**

* User registration.
* Autorization by login and password. Server should returns authentification token.
* Client connect to server using auth token. Server has to validate token as soon as user connected.
* User can ask for connected users list.
* Server protocol should provides to user an opportunity to send direct message and message to everyone.
