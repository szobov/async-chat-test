import os
from subprocess import PIPE, run

from setuptools import Command, find_packages, setup
from setuptools.command.sdist import sdist

NAME = 'chatty'
VERSION = '0.1'

REQUIRED = [
    'pyyaml',
    'aiohttp',
    'cerberus',
    'aioredis',
    'protobuf',
    'click',
    'frozendict'
]

DEV_REQUIRED = [
    'mypy',
    'flake8',
    'pycodestyle',
    'pep257',
]

TEST_REQUIRE = ['pytest', 'pytest-runner', 'pytest-aiohttp', 'pytest-mypy',
                'pytest-cov', 'pytest-flake8']


class ProtobufGenerateCommand(Command):
    """Setup command for generating protobuf python's files."""

    def initialize_options(self):
        self.__current_path = os.path.dirname(os.path.abspath(__file__))

    def finalize_options(self):
        pass

    def run(self):
        p = run(['protoc',
                 f'--python_out={NAME}/',
                 f'proto/api.proto'],
                stdout=PIPE, cwd=self.__current_path)
        err = "Check thet you have protobuf compiler installed installed"
        assert p.returncode == 0, err


class Sdist(sdist):
    """Extended sdist command that generate protobuf files before."""

    def run(self):
        self.run_command('protobuf_gen')
        sdist.run(self)


setup(
    name=NAME,
    version=VERSION,
    entry_points={
        'console_scripts': [
            f'{NAME}_run = {NAME}.__main__:main',
        ]
    },
    packages=find_packages(exclude=(f'{NAME}.tests', )),
    install_requires=REQUIRED,
    extras_require={
        'dev': DEV_REQUIRED
    },
    tests_require=TEST_REQUIRE,
    cmdclass={
        'protobuf_gen': ProtobufGenerateCommand,
        'sdist': Sdist
    }
)
