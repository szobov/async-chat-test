gen_proto:
	protoc --python_out=chatty/ proto/api.proto

all_checks:
	pytest --mypy --cov=chatty --flake8 .
