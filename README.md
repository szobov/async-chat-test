**About**

This project is my plaing with a new python's async-world, like aiohttp, aioredis.
Also, I decided to train with pytest and try to use protobuf.

Also, It's a tast assignment for a job application.
You can find task description in [TASK.md](https://gitlab.com/szobov/async-chat-test/blob/master/TASK.md).

**Prerequirements**

To run this project or test it, you need to have installed a [protobuf compiler](https://github.com/google/protobuf#protocol-compiler-installation).

**Test**

Run `python setup.py test` to run only tests or `make all_checks` to check types and guidestyles.
But you need to run `python setup.py sdist` before, it'll generate protobuf python bindings.

**Run**

If you have a [docker](https://docs.docker.com) and [docker-compose](https://docs.docker.com/compose/) you can easily run containers with chat server and redis.
Just run `docker-compose up`.

**ToDo**
* Cover more with tests and types.
